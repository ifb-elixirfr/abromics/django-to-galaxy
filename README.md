# 🛸 Django to Galaxy

The extension might be similar to [this project](https://github.com/computational-metabolomics/django-galaxy).

Django extension that eases communication with Galaxy instance to execute workflows.

This should includes:

* List available workflows on a Galaxy instance
* Add selected workflow from Galaxy instance to the app for execution from admin panel
* Run workflow from a Galaxy instance

For more information, please check the [Wiki](https://gitlab.com/ifb-elixirfr/abromics/django-to-galaxy/-/wikis/home)

-----------------------------------------

# Development

Packaging and organization of the library is done using [Poetry](https://python-poetry.org/docs/).

In brief after [installing](https://python-poetry.org/docs/#installation) poetry, from the repository
directory, just run:

```bash
poetry install
```

To activate the corresponding virtual environment created by poetry:

```bash
poetry self add poetry-plugin-shell
poetry shell
```

> 💬 *This can be useful to retrieve the absolute python path for VScode for instance (`which python`)*

## ✅ Run all quality checks and tests locally

You can run all quality checks and unit tests locally.

To list all available commands:

```bash
poetry run inv --list
# for instance
poetry run inv quality.all  # Run all quality tests
```

## 💻 Run the testing Django app locally

```bash
poetry self add poetry-plugin-shell # If not done previously
poetry shell
python testing_django_app/manage.py runserver
```