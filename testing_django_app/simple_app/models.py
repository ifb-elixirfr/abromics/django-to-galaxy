import os

from django.conf import settings
from django.db import models

DIR_PATH = os.path.join(settings.BASE_DIR, "testing_material")


class SomeInputFile(models.Model):
    name = models.CharField(max_length=100)
    file_path = models.FilePathField(path=DIR_PATH)
    file_type = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.name} ({self.id})"


class SomeOtherInputFile(models.Model):
    name = models.CharField(max_length=100)
    file = models.FileField(upload_to="other_input_files/")
    file_type = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.name} ({self.id})"
