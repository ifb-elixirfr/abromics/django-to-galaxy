# Generated by Django 4.1 on 2023-10-26 13:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("simple_app", "0004_alter_someinputfile_file_path"),
    ]

    operations = [
        migrations.AlterField(
            model_name="someinputfile",
            name="file_path",
            field=models.FilePathField(
                path="/home/jlao/django-to-galaxy/testing_django_app/testing_material"
            ),
        ),
    ]
