# Testing Django App

This is a testing Django app from the [Django tutorial](https://docs.djangoproject.com/en/4.0/intro/tutorial01/).

It is mainly use locally to have a django app to play with to test the `django_to_galaxy` extension.
