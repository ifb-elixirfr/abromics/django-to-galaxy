## User Story

> As a *`[who]`*, I want *`[what]`* so that *`[why]`*

> ✅  **How to fill in the story([source](https://www.leandog.com/blog/how-to-write-the-best-user-stories-with-story-cards))**
> * *`[who]`* will receive value from the feature? *e.g.: user, admin...*
> * *`[what]`*: A brief description the feature the user want. *e.g.: filter by xx, export in xx format...*
> * *`[why]`*: Even more important than what the feature is, we need to identify how that feature helps the user accomplish a goal. *e.g.: retrieve easily its data, access resultats outside of the app...*

## Basic requirements

*In a sentence or two, provide initial expectations for what this feature should be/do.*

## Acceptance criteria

*This is the basic benchmark that must be met to consider this card complete.*
