## Description

*Description of the new feature (if different from the one described in the corresponding issue)*

## Definition of Done

* [ ] New code added is tested
* [ ] Merge request has been revised and validated
* [ ] Wiki or documentation has been updated

## Related issue(s)

Closes *Issue number (e.g. #35)*
