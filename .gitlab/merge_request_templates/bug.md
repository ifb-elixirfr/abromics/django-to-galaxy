## Definition of Done

* [ ] Test has been added to reproduce and fix the bug (*only apply for Python at the moment*)
* [ ] Merge request has been revised and validated

## Related issue(s)

Closes *Issue number (e.g. #35)*
