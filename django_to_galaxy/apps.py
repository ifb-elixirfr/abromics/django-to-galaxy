from django.apps import AppConfig


class DjangoToGalaxyConfig(AppConfig):
    name = "django_to_galaxy"
    verbose_name = "Django to Galaxy"
