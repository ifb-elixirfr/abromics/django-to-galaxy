import factory
from factory.django import DjangoModelFactory

from django_to_galaxy.models.galaxy_user import GalaxyUser

from .galaxy_instance import GalaxyInstanceFactory


class GalaxyUserFactory(DjangoModelFactory):
    class Meta:
        model = GalaxyUser

    email = factory.Faker("email")
    api_key = factory.Faker("md5")
    galaxy_instance = factory.SubFactory(GalaxyInstanceFactory)
