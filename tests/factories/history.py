import pytz

import factory

from django_to_galaxy.models.history import History

from .galaxy_element import GalaxyElementFactory


class HistoryFactory(GalaxyElementFactory):
    class Meta:
        model = History

    name = factory.Faker("bothify", text="History-?###")
    galaxy_state = "new"
    create_time = factory.Faker("date_time", tzinfo=pytz.UTC)
