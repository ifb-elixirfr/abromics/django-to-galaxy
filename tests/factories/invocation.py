import pytz

import factory
from factory.django import DjangoModelFactory

from django_to_galaxy.models.invocation import Invocation

from .history import HistoryFactory
from .workflow import WorkflowFactory


class InvocationFactory(DjangoModelFactory):
    class Meta:
        model = Invocation

    galaxy_id = factory.Faker("md5")
    galaxy_state = "new"
    workflow = factory.SubFactory(WorkflowFactory)
    history = factory.SubFactory(HistoryFactory)
    create_time = factory.Faker("date_time", tzinfo=pytz.UTC)
