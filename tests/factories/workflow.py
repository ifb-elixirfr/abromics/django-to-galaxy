import factory

from django_to_galaxy.models.workflow import Workflow

from .galaxy_element import GalaxyElementFactory


class WorkflowFactory(GalaxyElementFactory):
    class Meta:
        model = Workflow

    name = factory.Faker("bothify", text="Workflow-?###")
