import factory
from factory.django import DjangoModelFactory

from django_to_galaxy.models.galaxy_instance import GalaxyInstance


class GalaxyInstanceFactory(DjangoModelFactory):
    class Meta:
        model = GalaxyInstance

    name = factory.Faker("bothify", text="Galaxy-?###")
    url = factory.LazyAttribute(lambda o: f"http://{o.name.lower()}.com")
