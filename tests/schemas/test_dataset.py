import unittest

from django_to_galaxy.schemas.dataset import SimpleDataset


class TestSimpleDataset(unittest.TestCase):
    def test_generate_datamap(self):
        # Given
        tool_id = "tool_id"
        dataset_id = "data_id"
        # When
        test_dataset = SimpleDataset(id=dataset_id, name="name", data_type="osef")
        expected_datamap = {tool_id: {"id": dataset_id, "src": "hda"}}
        # Then
        self.assertDictEqual(test_dataset.generate_datamap(tool_id), expected_datamap)
