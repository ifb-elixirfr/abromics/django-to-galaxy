from django.test import TestCase, override_settings
from rest_framework.test import APIClient

from tests.factories.history import HistoryFactory


@override_settings(ROOT_URLCONF="testing_django_app.testing_django_app.urls")
class TestHistoryAPI(TestCase):
    def setUp(self):
        self.batch_number = 4
        self.instances = HistoryFactory.create_batch(self.batch_number)
        self.client = APIClient()

    def test_get_all_instances(self):
        # When
        t = self.client.get("/django_to_galaxy/api/histories/")
        # Then
        self.assertEqual(len(t.data), self.batch_number)

    def test_get_instance(self):
        # Given
        expected_instance = self.instances[0]
        instance_id = self.instances[0].galaxy_id
        # When
        t = self.client.get(f"/django_to_galaxy/api/histories/{instance_id}/")
        data = t.data
        for key in data.keys():
            if key in ("galaxy_owner",):
                continue
            self.assertEqual(data[key], getattr(expected_instance, key))
