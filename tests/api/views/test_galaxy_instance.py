from django.test import TestCase, override_settings
from rest_framework.test import APIClient

from django_to_galaxy.api.serializers.galaxy_instance import GalaxyInstanceSerializer
from django_to_galaxy.models.galaxy_instance import GalaxyInstance

from tests.factories.galaxy_instance import GalaxyInstanceFactory


@override_settings(ROOT_URLCONF="testing_django_app.testing_django_app.urls")
class TestGalaxyInstanceAPI(TestCase):
    def setUp(self):
        self.batch_number = 10
        self.instances = GalaxyInstanceFactory.create_batch(self.batch_number)
        self.client = APIClient()

    def test_get_all_instances(self):
        # When
        t = self.client.get("/django_to_galaxy/api/instances/")
        # Then
        self.assertEqual(len(t.data), self.batch_number)

    def test_get_instance(self):
        # Given
        expected_instance = self.instances[0]
        instance_id = self.instances[0].id
        # When
        t = self.client.get(f"/django_to_galaxy/api/instances/{instance_id}/")
        data = t.data
        for key in data.keys():
            self.assertEqual(data[key], getattr(expected_instance, key))

    def test_post_instance(self):
        # Given
        post_data = {"url": "http://jojogalaxyterrible.fr", "name": "Terrible"}
        # Then
        self.assertEqual(GalaxyInstance.objects.all().count(), self.batch_number)
        self.client.post("/django_to_galaxy/api/instances/", data=post_data)
        self.assertEqual(GalaxyInstance.objects.all().count(), self.batch_number + 1)

    def test_put_instance(self):
        # Given
        old_instance = self.instances[0]
        old_id = old_instance.id
        serializer = GalaxyInstanceSerializer(old_instance)
        new_data = serializer.data
        new_name = "Tomato"
        new_data["name"] = new_name
        # Then
        self.assertNotEqual(GalaxyInstance.objects.get(id=old_id).name, new_name)
        self.client.put(f"/django_to_galaxy/api/instances/{old_id}/", data=new_data)
        self.assertEqual(GalaxyInstance.objects.get(id=old_id).name, new_name)

    def test_delete_instance(self):
        # Given
        to_delete_id = self.instances[0].id
        # Then
        self.assertEqual(GalaxyInstance.objects.all().count(), self.batch_number)
        self.client.delete(f"/django_to_galaxy/api/instances/{to_delete_id}/")
        self.assertEqual(GalaxyInstance.objects.all().count(), self.batch_number - 1)
