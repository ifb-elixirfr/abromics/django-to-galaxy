from django.test import TestCase, override_settings
from rest_framework.test import APIClient

from tests.factories.history import HistoryFactory
from tests.factories.workflow import WorkflowFactory


@override_settings(ROOT_URLCONF="testing_django_app.testing_django_app.urls")
class TestUploadToHistoryView(TestCase):
    def setUp(self):
        self.history = HistoryFactory()
        self.client = APIClient()

    def test_post_wrong_data_input(self):
        # Given
        post_data = {"wrong": "data", "name": "Terrible"}
        # Then
        response = self.client.post(
            "/django_to_galaxy/api/upload_to_history/", data=post_data
        )
        self.assertEqual(response.status_code, 400)

    def test_post_unexisting_model(self):
        # Given
        post_data = {
            "history_id": self.history.id,
            "file": {"model": "idonnotexist", "path_field": "file_path", "file_type": "txt","id": 123},
        }
        # Then
        response = self.client.post(
            "/django_to_galaxy/api/upload_to_history/", data=post_data, format="json"
        )
        self.assertEqual(response.status_code, 400)

    def test_post_unexisting_history(self):
        # Given
        post_data = {
            "history_id": 123456,
            "file": {"model": "user", "path_field": "file_path", "file_type": "txt", "id": 123},
        }
        # Then
        response = self.client.post(
            "/django_to_galaxy/api/upload_to_history/", data=post_data, format="json"
        )
        self.assertEqual(response.status_code, 404)

    def test_post_unexisting_file(self):
        # Given
        post_data = {
            "history_id": self.history.id,
            "file": {"model": "user", "path_field": "file_path", "file_type": "txt", "id": 123},
        }
        # Then
        response = self.client.post(
            "/django_to_galaxy/api/upload_to_history/", data=post_data, format="json"
        )
        self.assertEqual(response.status_code, 404)

    def test_post_unexisting_field_path_field(self):
        # Given
        workflow = WorkflowFactory()
        post_data = {
            "history_id": self.history.id,
            "file": {"model": "workflow", "path_field": "file_path", "file_type": "test", "id": workflow.id},
        }
        # Then
        response = self.client.post(
            "/django_to_galaxy/api/upload_to_history/", data=post_data, format="json"
        )
        self.assertEqual(response.status_code, 404)

    def test_post_unexisting_file_type(self):
        # Given
        post_data = {
            "history_id": self.history.id,
            "file": {"model": "workflow", "path_field": "file_path", "file_type": "txt", "id": 123},
        }
        # Then
        response = self.client.post(
            "/django_to_galaxy/api/upload_to_history/", data=post_data, format="json"
        )
        self.assertEqual(response.status_code, 404)

    def test_post(self):
        """@TODO mock a class that is used to deal with files."""
        # Given
        # Then
        # self.assertEqual(response.status_code, 200)
        pass
