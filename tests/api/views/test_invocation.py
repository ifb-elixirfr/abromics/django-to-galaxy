from unittest.mock import patch, Mock

from django.test import TestCase, override_settings
from rest_framework.test import APIClient

from django_to_galaxy.models import Invocation

from tests.factories.invocation import InvocationFactory


@override_settings(ROOT_URLCONF="testing_django_app.testing_django_app.urls")
class TestInvocationAPI(TestCase):
    def setUp(self):
        self.batch_number = 4
        self.instances = InvocationFactory.create_batch(self.batch_number)
        self.client = APIClient()

    def test_get_all_instances(self):
        # When
        t = self.client.get("/django_to_galaxy/api/invocations/")
        # Then
        self.assertEqual(len(t.data), self.batch_number)

    def test_get_instance(self):
        # Given
        expected_instance = self.instances[0]
        instance_id = self.instances[0].id
        # When
        t = self.client.get(f"/django_to_galaxy/api/invocations/{instance_id}/")
        data = t.data
        for key in data.keys():
            if key in ("workflow", "history", "create_time"):
                continue
            self.assertEqual(data[key], getattr(expected_instance, key))


@override_settings(ROOT_URLCONF="testing_django_app.testing_django_app.urls")
class TestUpdateOutputFilesView(TestCase):
    def setUp(self):
        self.invocation = InvocationFactory()
        self.client = APIClient()

    def test_get_unexisting_entry(self):
        # Given
        unexisting_inv_id = 123
        # Then
        response = self.client.get(
            f"/django_to_galaxy/api/update_galaxy_output_files/{unexisting_inv_id}",
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data["detail"].code, "not_found")

    def test_get(self):
        # Given
        invocation_id = self.invocation.id
        # Then
        with patch.object(
            Invocation, "update_output_files"
        ) as mock_update_output_files:
            mock_update_output_files.return_value = None
            response = self.client.get(
                f"/django_to_galaxy/api/update_galaxy_output_files/{invocation_id}",
            )
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data["galaxy_output_file_ids"], [])
