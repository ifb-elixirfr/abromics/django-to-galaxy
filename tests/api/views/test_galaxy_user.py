from django.test import TestCase, override_settings
from rest_framework.test import APIClient

from django_to_galaxy.api.serializers.galaxy_user import GalaxyUserSerializer
from django_to_galaxy.models.galaxy_user import GalaxyUser

from tests.factories.galaxy_user import GalaxyUserFactory


@override_settings(ROOT_URLCONF="testing_django_app.testing_django_app.urls")
class TestGalaxyUserAPI(TestCase):
    def setUp(self):
        self.batch_number = 4
        self.instances = GalaxyUserFactory.create_batch(self.batch_number)
        self.client = APIClient()

    def test_get_all_instances(self):
        # When
        t = self.client.get("/django_to_galaxy/api/galaxy_users/")
        # Then
        self.assertEqual(len(t.data), self.batch_number)

    def test_get_instance(self):
        # Given
        expected_instance = self.instances[0]
        instance_id = self.instances[0].id
        # When
        t = self.client.get(f"/django_to_galaxy/api/galaxy_users/{instance_id}/")
        data = t.data
        for key in data.keys():
            if key in ("galaxy_instance",):
                continue
            self.assertEqual(data[key], getattr(expected_instance, key))

    def test_post_instance(self):
        # Given
        post_data = {
            "email": "super@user.bzh",
            "galaxy_instance": self.instances[0].galaxy_instance.id,
        }
        # Then
        self.assertEqual(GalaxyUser.objects.all().count(), self.batch_number)
        self.client.post("/django_to_galaxy/api/galaxy_users/", data=post_data)
        self.assertEqual(GalaxyUser.objects.all().count(), self.batch_number + 1)

    def test_put_instance(self):
        # Given
        old_instance = self.instances[0]
        old_id = old_instance.id
        serializer = GalaxyUserSerializer(old_instance)
        new_data = serializer.data
        new_data["galaxy_instance"] = new_data["galaxy_instance"]["id"]
        new_email = "jojo@bibi.bzh"
        new_data["email"] = new_email
        # Then
        self.assertNotEqual(GalaxyUser.objects.get(id=old_id).email, new_email)
        self.client.put(f"/django_to_galaxy/api/galaxy_users/{old_id}/", data=new_data)
        self.assertEqual(GalaxyUser.objects.get(id=old_id).email, new_email)

    def test_delete_instance(self):
        # Given
        to_delete_id = self.instances[0].id
        # Then
        self.assertEqual(GalaxyUser.objects.all().count(), self.batch_number)
        self.client.delete(f"/django_to_galaxy/api/galaxy_users/{to_delete_id}/")
        self.assertEqual(GalaxyUser.objects.all().count(), self.batch_number - 1)
