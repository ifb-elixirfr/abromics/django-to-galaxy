from unittest.mock import patch, Mock

from django.test import TestCase, override_settings
from rest_framework.test import APIClient

from django_to_galaxy.models import GalaxyUser

from tests.factories.galaxy_user import GalaxyUserFactory


@override_settings(ROOT_URLCONF="testing_django_app.testing_django_app.urls")
class TestCreateHistoryView(TestCase):
    def setUp(self):
        self.galaxy_user = GalaxyUserFactory()
        self.client = APIClient()

    def test_get_unexisting_entry(self):
        # Given
        unexisting_user_id = 123
        # Then
        response = self.client.get(
            f"/django_to_galaxy/api/create_history/{unexisting_user_id}",
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data["detail"].code, "not_found")


    def test_get(self):
        # Given
        galaxy_user_id = self.galaxy_user.id
        # When
        mocked_history = Mock()
        mocked_history.id = 3556
        # Then
        with patch.object(GalaxyUser, "create_history") as mock_create_history:
            mock_create_history.return_value = mocked_history
            response = self.client.get(
                f"/django_to_galaxy/api/create_history/{galaxy_user_id}",
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["history_id"], 3556)
