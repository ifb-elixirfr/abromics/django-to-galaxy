from unittest.mock import patch, Mock, MagicMock

from django.test import TestCase, override_settings
from rest_framework.test import APIClient

from django_to_galaxy.models import Workflow

from tests.factories.history import HistoryFactory
from tests.factories.workflow import WorkflowFactory


@override_settings(ROOT_URLCONF="testing_django_app.testing_django_app.urls")
class TestUploadToHistoryView(TestCase):
    def setUp(self):
        self.history = HistoryFactory()
        self.workflow = WorkflowFactory()
        self.client = APIClient()

    def test_post_wrong_data_input(self):
        # Given
        post_data = {"wrong": "data", "name": "Terrible"}
        # Then
        response = self.client.post(
            "/django_to_galaxy/api/invoke_workflow/", data=post_data
        )
        self.assertEqual(response.status_code, 400)

    def test_post_unexisting_history(self):
        # Given
        post_data = {
            "history_id": 123456,
            "workflow_id": self.workflow.id,
            "datamap": {"0": {"id": "pouet", "src": "hda"}},
        }
        # Then
        response = self.client.post(
            "/django_to_galaxy/api/invoke_workflow/", data=post_data, format="json"
        )
        self.assertEqual(response.status_code, 404)

    def test_post_unexisting_workflow(self):
        # Given
        post_data = {
            "history_id": self.history.id,
            "workflow_id": 123465,
            "datamap": {"0": {"id": "pouet", "src": "hda"}},
        }
        # Then
        response = self.client.post(
            "/django_to_galaxy/api/invoke_workflow/", data=post_data, format="json"
        )
        self.assertEqual(response.status_code, 404)

    def test_post(self):
        # Given
        post_data = {
            "history_id": self.history.id,
            "workflow_id": self.workflow.id,
            "datamap": {"0": {"id": "pouet", "src": "hda"}},
        }
        # When
        expected_message = "Workflow successfully invoked."
        fake_invocation_id = 123
        mocked_invocation = Mock()
        mocked_invocation.id = fake_invocation_id
        # Then
        with patch.object(Workflow, "invoke") as mock_invoke:
            mock_invoke.return_value = mocked_invocation
            response = self.client.post(
                "/django_to_galaxy/api/invoke_workflow/", data=post_data, format="json"
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["invocation_id"], fake_invocation_id)
        self.assertEqual(response.data["message"], expected_message)


@override_settings(ROOT_URLCONF="testing_django_app.testing_django_app.urls")
class TestGetWorkflowDatamapTemplateView(TestCase):
    def setUp(self):
        self.workflow = WorkflowFactory()
        self.client = APIClient()

    def test_get_unexisting_entry(self):
        # Given
        unexisting_workflow_id = 123
        # Then
        response = self.client.get(
            f"/django_to_galaxy/api/get_datamap_template/{unexisting_workflow_id}",
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data["detail"].code, "not_found")

    def test_get(self):
        # Given
        workflow_id = self.workflow.id
        # When
        input_id = 0
        input_label = "input"
        input_type = "input_type"
        tool_inputs = {}
        mocked_galaxy_workflow = Mock()
        mocked_galaxy_workflow.inputs = {
            input_id: {"label": input_label, "other_info": "pouet"}
        }
        mocked_galaxy_workflow.steps = {}
        mocked_galaxy_workflow.steps[input_id] = MagicMock()
        mocked_galaxy_workflow.steps[input_id].type = input_type
        mocked_galaxy_workflow.steps[input_id].tool_inputs = tool_inputs

        expected_data = {
            "input_mapping": {input_id: {"label": input_label, "type": input_type, "tool_inputs": tool_inputs}},
            "datamap_template": {input_id: {"id": "", "src": "hda"}},
        }
        # Then
        with patch.object(Workflow, "_get_galaxy_workflow") as mock_get_galaxy_workflow:
            mock_get_galaxy_workflow.return_value = mocked_galaxy_workflow
            response = self.client.get(
                f"/django_to_galaxy/api/get_datamap_template/{workflow_id}",
            )
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, expected_data)
