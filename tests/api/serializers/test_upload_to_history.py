import unittest

from django_to_galaxy.api.serializers.upload_to_history import FileSerializer


class TestFileSerializer(unittest.TestCase):
    def test_validate_unexisting_model(self):
        # Given
        data = {"model": "idonotexist", "path_field": "file_path", "file_type": "txt", "id": 123}
        # When
        test_serializer = FileSerializer(data=data)
        # Then
        self.assertFalse(test_serializer.is_valid())

    def test_validate_existing_model(self):
        # Given
        data = {"model": "user", "path_field": "file_path", "file_type": "txt", "id": 123}
        # When
        test_serializer = FileSerializer(data=data)
        # Then
        self.assertTrue(test_serializer.is_valid())
