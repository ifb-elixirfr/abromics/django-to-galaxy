from unittest.mock import patch, Mock

from django.test import TestCase

from django_to_galaxy.models.galaxy_instance import GalaxyInstance

from tests.factories.galaxy_instance import GalaxyInstanceFactory


class TestGalaxyInstance(TestCase):
    def setUp(self):
        self.test_gi = GalaxyInstanceFactory()

    def test_str(self):
        galaxy_test = GalaxyInstance.objects.get(name=self.test_gi.name)
        self.assertEqual(str(galaxy_test), f"{self.test_gi.name} [{self.test_gi.url}]")

    def test_is_online_true(self):
        # Given
        galaxy_test = GalaxyInstance.objects.get(name=self.test_gi.name)
        # When
        mock_response = Mock()
        mock_response.raise_for_status.return_value = None
        mock_requests = Mock()
        mock_requests.get.return_value = mock_response
        with patch(
            "django_to_galaxy.models.galaxy_instance.requests",
            mock_requests,
        ):
            # Then
            self.assertTrue(galaxy_test.is_online())

    def test_is_online_false(self):
        # Given
        galaxy_test = GalaxyInstance.objects.get(name=self.test_gi.name)
        # When
        mock_response = Mock()
        mock_response.raise_for_status.side_effect = Exception()
        mock_requests = Mock()
        mock_requests.get.return_value = mock_response
        with patch(
            "django_to_galaxy.models.galaxy_instance.requests",
            mock_requests,
        ):
            # Then
            self.assertFalse(galaxy_test.is_online())
