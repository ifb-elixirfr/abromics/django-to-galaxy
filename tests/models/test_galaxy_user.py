from unittest.mock import MagicMock

from django.test import TestCase
from freezegun import freeze_time

from django_to_galaxy.models.galaxy_user import GalaxyUser
from django_to_galaxy.models.history import History
from django_to_galaxy.models.workflow import Workflow
from django_to_galaxy.models.galaxy_element import Tag

from tests.factories.utils import generate_galaxy_time
from tests.factories.galaxy_user import GalaxyUserFactory


class TestGalaxyUser(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.galaxy_user = GalaxyUserFactory()

    def test_get_bioblend_gi(self):
        # Given
        user = GalaxyUser.objects.get(email=self.galaxy_user.email)
        # When
        bioblend_gi = user.get_bioblend_obj_gi()
        # Then
        self.assertEqual(bioblend_gi.gi.base_url, user.galaxy_instance.url)
        self.assertEqual(bioblend_gi.gi._key, user.api_key)

    def test_get_available_workflows(self):
        # Given
        user = GalaxyUser.objects.get(email=self.galaxy_user.email)
        # When
        # - Mock workflow
        mock_workflow = MagicMock()
        mock_workflow.id = "galaxy_wf_id"
        mock_workflow.name = "galaxy_wf_name"
        mock_workflow.published = False
        mock_workflow.wrapped = {"annotation": "test", "tags": []}
        # - Mock obj_gi
        mock_obj_gi = MagicMock()
        mock_obj_gi.workflows.list.return_value = [mock_workflow]
        user.obj_gi = mock_obj_gi
        expected_workflow = Workflow(
            galaxy_owner=user,
            galaxy_id="galaxy_wf_id",
            name="galaxy_wf_name",
            published=False,
            annotation="test",
        )
        # Then
        av_workflows = user.get_available_workflows()
        self.assertEqual(len(av_workflows), 1)
        for field in ["galaxy_id", "name", "published", "annotation"]:
            self.assertEqual(
                getattr(av_workflows[0][0], field), getattr(expected_workflow, field)
            )

    def test_available_workflows(self):
        # Given
        user = GalaxyUser.objects.get(email=self.galaxy_user.email)
        # When
        # - Mock workflow
        mock_workflow = MagicMock()
        mock_workflow.id = "galaxy_wf_id"
        mock_workflow.name = "galaxy_wf_name"
        mock_workflow.published = False
        mock_workflow.wrapped = {"annotation": "test", "tags": []}
        # - Mock obj_gi
        mock_obj_gi = MagicMock()
        mock_obj_gi.workflows.list.return_value = [mock_workflow]
        user.obj_gi = mock_obj_gi
        expected_workflow = Workflow(
            galaxy_owner=user,
            galaxy_id="galaxy_wf_id",
            name="galaxy_wf_name",
            published=False,
            annotation="test",
        )
        # Then
        av_workflows = user.available_workflows
        self.assertEqual(len(av_workflows), 1)
        for field in ["galaxy_id", "name", "published", "annotation"]:
            self.assertEqual(
                getattr(av_workflows[0][0], field), getattr(expected_workflow, field)
            )

    def test_obj_gi(self):
        # Given
        user = GalaxyUser.objects.get(email=self.galaxy_user.email)
        # When
        bioblend_gi = user.obj_gi
        # Then
        self.assertEqual(bioblend_gi.gi.base_url, user.galaxy_instance.url)
        self.assertEqual(bioblend_gi.gi._key, user.api_key)

    def test_str(self):
        # Given
        user = GalaxyUser.objects.get(email=self.galaxy_user.email)
        # Then
        self.assertEqual(str(user), f"{user.email} at {user.galaxy_instance}")

    @freeze_time("2022-01-29")
    def test_generate_history_name(self):
        # Given
        user = GalaxyUser.objects.get(email=self.galaxy_user.email)
        # When
        expected_history_name = "20220129-000000_created_by_django_to_galaxy"
        # Then
        self.assertEqual(user._generate_history_name(), expected_history_name)

    def test_create_history(self):
        # Given
        user = GalaxyUser.objects.get(email=self.galaxy_user.email)
        # When
        # - Mock history
        mock_history = MagicMock()
        mock_history.id = "galaxy_history_id"
        mock_history.name = "galaxy_history_name"
        mock_history.published = False
        mock_history.wrapped = {"key": "test", "create_time": generate_galaxy_time()}
        mock_history.state = "new"
        # - Mock obj_gi
        mock_obj_gi = MagicMock()
        mock_obj_gi.histories.create.return_value = mock_history
        user.obj_gi = mock_obj_gi
        self.assertFalse(History.objects.all())
        # Then
        user.create_history()
        self.assertEqual(len(History.objects.all()), 1)
