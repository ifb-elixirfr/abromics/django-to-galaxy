from unittest.mock import Mock, MagicMock

from django.test import TestCase

from django_to_galaxy.models.history import History
from django_to_galaxy.schemas.dataset import SimpleDataset

from tests.factories.history import HistoryFactory


class TestHistory(TestCase):
    def setUp(self):
        self.history = HistoryFactory()

    def test_galaxy_history(self):
        # Given
        history = History.objects.get(galaxy_id=self.history.galaxy_id)
        # When
        # - Mock obj_gi
        mock_obj_gi = MagicMock()
        mock_obj_gi.histories.get.return_value = True
        history.galaxy_owner.obj_gi = mock_obj_gi
        # Then
        self.assertTrue(history.galaxy_history)
        mock_obj_gi.histories.get.assert_called_once()

    def test_simplify_datasets(self):
        # Given
        history = History.objects.get(galaxy_id=self.history.galaxy_id)
        # When
        # - Mock galaxy_history
        test_dataset = {
            "id": "id",
            "name": "name",
            "data_type": "test_data",
            "useless_key": "2229",
        }
        mock_dataset = Mock()
        mock_dataset.wrapped = test_dataset
        mock_galaxy_history = MagicMock()
        mock_galaxy_history.get_datasets.return_value = [mock_dataset]
        history._galaxy_history = mock_galaxy_history
        expected_dataset = SimpleDataset(id="id", name="name", data_type="test_data")
        # Then
        simplified_datasets = history.simplify_datasets
        self.assertEqual(len(simplified_datasets), 1)
        self.assertEqual(simplified_datasets[0], expected_dataset)

    def test_delete(self):
        # Given
        history = History.objects.get(galaxy_id=self.history.galaxy_id)
        # When
        # - Mock obj_gi
        mock_obj_gi = MagicMock()
        mock_obj_gi.histories.delete.return_value = None
        history.galaxy_owner.obj_gi = mock_obj_gi
        self.assertEqual(len(History.objects.all()), 1)
        # Then
        history.delete()
        self.assertFalse(len(History.objects.all()))
        mock_obj_gi.histories.delete.assert_called_once()

    def test_synchronize(self):
        # Given
        history = History.objects.get(galaxy_id=self.history.galaxy_id)
        # When
        # - Mock galaxy_history
        mock_galaxy_history = Mock()
        mock_galaxy_history.name = "new_name"
        mock_galaxy_history.annotation = "new_annot"
        mock_galaxy_history.published = history.published
        mock_galaxy_history.state = history.galaxy_state
        mock_get_galaxy_history = Mock()
        mock_get_galaxy_history.return_value = mock_galaxy_history
        history._get_galaxy_history = mock_get_galaxy_history
        # Then
        self.assertNotEqual(history.name, "new_name")
        self.assertNotEqual(history.annotation, "new_annot")
        history.synchronize()
        self.assertEqual(history.name, "new_name")
        self.assertEqual(history.annotation, "new_annot")

    def test_upload_file(self):
        # Given
        history = History.objects.get(galaxy_id=self.history.galaxy_id)
        # When
        # - Mock galaxy_history
        mock_galaxy_history = Mock()
        mock_galaxy_history.upload_file.return_value = MagicMock()
        history._galaxy_history = mock_galaxy_history
        # Then
        history.upload_file("non_existing_file.txt")
        mock_galaxy_history.upload_file.assert_called_once()
        mock_galaxy_history.upload_file.assert_called_with("non_existing_file.txt")

    def test_str(self):
        # Given
        history = History.objects.get(galaxy_id=self.history.galaxy_id)
        # Then
        self.assertEqual(
            str(history), f"{self.history.name} ({self.history.galaxy_id})"
        )

    def test_repr(self):
        # Given
        history = History.objects.get(galaxy_id=self.history.galaxy_id)
        # Then
        self.assertEqual(
            repr(history), f"History: {self.history.name} ({self.history.galaxy_id})"
        )
