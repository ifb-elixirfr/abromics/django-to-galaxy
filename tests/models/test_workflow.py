from unittest.mock import Mock, MagicMock, patch

from django.test import TestCase

from django_to_galaxy.models.workflow import Workflow

from tests.factories.utils import generate_galaxy_time
from tests.factories.workflow import WorkflowFactory


class TestWorkflow(TestCase):
    def setUp(self):
        self.workflow = WorkflowFactory()

    def test_galaxy_workflow(self):
        # Given
        wf = Workflow.objects.get(galaxy_id=self.workflow.galaxy_id)
        # When
        # - Mock obj_gi
        mock_obj_gi = MagicMock()
        mock_obj_gi.workflows.get.return_value = True
        wf.galaxy_owner.obj_gi = mock_obj_gi
        # Then
        self.assertTrue(wf.galaxy_workflow)
        mock_obj_gi.workflows.get.assert_called_once()

    def test_invoke(self):
        # Given
        wf = Workflow.objects.get(galaxy_id=self.workflow.galaxy_id)
        # When
        # - Mock galaxy workflow
        mock_galaxy_wf = MagicMock()
        mock_galaxy_inv = Mock()
        mock_galaxy_inv.wrapped = {"create_time": generate_galaxy_time()}
        mock_galaxy_wf.invoke.return_value = mock_galaxy_inv
        wf._galaxy_workflow = mock_galaxy_wf
        datamap = {"test": 1}
        history = Mock()
        # - Mock Invocation models and method
        mock_invocation = Mock()
        mock_invocation_instance = Mock()
        mock_invocation_instance.save = Mock()
        mock_invocation.return_value = mock_invocation_instance
        # Then
        with patch("django_to_galaxy.models.workflow.Invocation", mock_invocation):
            wf.invoke(datamap, history=history)
        mock_galaxy_wf.invoke.assert_called_once()
        mock_invocation.assert_called_once()
        mock_invocation_instance.save.assert_called_once()

    def test_str(self):
        # Given
        workflow = Workflow.objects.get(galaxy_id=self.workflow.galaxy_id)
        # Then
        self.assertEqual(
            str(workflow), f"{self.workflow.name} ({self.workflow.galaxy_id})"
        )

    def test_repr(self):
        # Given
        workflow = Workflow.objects.get(galaxy_id=self.workflow.galaxy_id)
        # Then
        self.assertEqual(
            repr(workflow),
            f"Workflow: {self.workflow.name} ({self.workflow.galaxy_id})",
        )
