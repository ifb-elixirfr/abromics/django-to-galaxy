from unittest.mock import Mock, patch

from django.test import TestCase
from django.contrib import messages
from django.contrib.admin.sites import AdminSite

from django_to_galaxy.admin.invocation import InvocationAdmin
from django_to_galaxy.models import (
    Invocation,
)

from tests.factories.galaxy_output_file import GalaxyOutputFileFactory
from tests.factories.invocation import InvocationFactory


SINGULAR_STR = "singular"
PLURAL_STR = "plural"


class MockRequest:
    pass


class TestHistoryAdmin(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.invocation = InvocationFactory()
        cls.invocation_one_file = InvocationFactory()
        GalaxyOutputFileFactory(invocation=cls.invocation_one_file)
        cls.invocation_two_files = InvocationFactory()
        GalaxyOutputFileFactory.create_batch(2, invocation=cls.invocation_two_files)

    def setUp(self):
        self.site = AdminSite()

    def test_get_message_singular_invocation(self):
        # Given
        galaxy_invocation_admin = InvocationAdmin(Invocation, self.site)
        # When
        expected_string = f"1 {SINGULAR_STR} [{self.invocation.galaxy_id} [{self.invocation.workflow.name}]]."
        # - mock message_user method
        galaxy_invocation_admin.message_user = Mock()
        galaxy_invocation_admin.message_user.return_value = None
        # Then
        galaxy_invocation_admin._get_message_invocation(
            MockRequest(),
            [self.invocation],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_invocation_admin.message_user.call_args_list[0][0][1],
            expected_string,
        )

    def test_get_message_plural_invocation(self):
        # Given
        galaxy_invocation_admin = InvocationAdmin(Invocation, self.site)
        # When
        expected_string = (
            f"2 {PLURAL_STR} [{self.invocation.galaxy_id} [{self.invocation.workflow.name}], "
            f"{self.invocation.galaxy_id} [{self.invocation.workflow.name}]]."
        )
        # - mock message_user method
        galaxy_invocation_admin.message_user = Mock()
        galaxy_invocation_admin.message_user.return_value = None
        # Then
        galaxy_invocation_admin._get_message_invocation(
            MockRequest(),
            [self.invocation, self.invocation],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_invocation_admin.message_user.call_args_list[0][0][1],
            expected_string,
        )

    def test_get_message_output_files_singular(self):
        # Given
        galaxy_invocation_admin = InvocationAdmin(Invocation, self.site)
        # When
        expected_string = f"1 {SINGULAR_STR} [{self.invocation_one_file.galaxy_id} [{self.invocation_one_file.workflow.name}]]."
        # - mock message_user method
        galaxy_invocation_admin.message_user = Mock()
        galaxy_invocation_admin.message_user.return_value = None
        # Then
        galaxy_invocation_admin._get_message_output_files(
            MockRequest(),
            [self.invocation_one_file],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_invocation_admin.message_user.call_args_list[0][0][1],
            expected_string,
        )

    def test_get_message_output_files_plural(self):
        # Given
        galaxy_invocation_admin = InvocationAdmin(Invocation, self.site)
        # When
        expected_string = f"2 {PLURAL_STR} [{self.invocation_two_files.galaxy_id} [{self.invocation_two_files.workflow.name}]]."
        # - mock message_user method
        galaxy_invocation_admin.message_user = Mock()
        galaxy_invocation_admin.message_user.return_value = None
        # Then
        galaxy_invocation_admin._get_message_output_files(
            MockRequest(),
            [self.invocation_two_files],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_invocation_admin.message_user.call_args_list[0][0][1],
            expected_string,
        )

    def test_synchronize_invocation(self):
        # Given
        galaxy_invocation_admin = InvocationAdmin(Invocation, self.site)
        # When
        to_success_action = Mock()
        to_success_action.synchronize.return_value = None
        to_fail_action = Mock()
        to_fail_action.synchronize.side_effect = Exception()
        fake_queryset = [to_success_action, to_fail_action]
        # - mock method for message
        galaxy_invocation_admin._get_message_invocation = Mock()
        galaxy_invocation_admin._get_message_invocation.return_value = None
        # - call the method
        galaxy_invocation_admin.synchronize_invocation(MockRequest(), fake_queryset)
        # Then
        to_success_action.synchronize.assert_called_once()
        to_fail_action.synchronize.assert_called_once()
        galaxy_invocation_admin._get_message_invocation.assert_called()
        self.assertEqual(galaxy_invocation_admin._get_message_invocation.call_count, 2)

    def test_update_output_files(self):
        # Given
        galaxy_invocation_admin = InvocationAdmin(Invocation, self.site)
        # When
        to_success_action = Mock()
        to_success_action.update_output_files.return_value = None
        to_fail_action = Mock()
        to_fail_action.update_output_files.side_effect = Exception()
        fake_queryset = [to_success_action, to_fail_action]
        # - mock method for message
        galaxy_invocation_admin._get_message_output_files = Mock()
        galaxy_invocation_admin._get_message_output_files.return_value = None
        # - call the method
        galaxy_invocation_admin.update_output_files(MockRequest(), fake_queryset)
        # Then
        to_success_action.update_output_files.assert_called_once()
        to_fail_action.update_output_files.assert_called_once()
        galaxy_invocation_admin._get_message_output_files.assert_called()
        self.assertEqual(
            galaxy_invocation_admin._get_message_output_files.call_count, 2
        )

    def test_get_number_output_files(self):
        # Given
        galaxy_invocation_admin = InvocationAdmin(Invocation, self.site)
        # When
        link_str = "link_history"
        expected_link = (
            f'<a href="{link_str}?invocation__id={self.invocation.id}">0</a>'
        )
        mock_reverse = Mock()
        mock_reverse.return_value = link_str
        with patch("django_to_galaxy.admin.invocation.reverse", mock_reverse):
            # Then
            self.assertEqual(
                galaxy_invocation_admin.get_number_output_files(self.invocation),
                expected_link,
            )
