from django.test import TestCase
from django.contrib.admin.sites import AdminSite

from django_to_galaxy.admin.galaxy_element import GalaxyElementAdmin
from django_to_galaxy.models.galaxy_element import GalaxyElement


class TestGalaxyElementAdmin(TestCase):
    def setUp(self):
        self.site = AdminSite()

    def test_has_add_permission(self):
        # Given
        galaxy_element_admin = GalaxyElementAdmin(GalaxyElement, self.site)
        # Then
        self.assertFalse(galaxy_element_admin.has_add_permission())
