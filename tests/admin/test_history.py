from unittest.mock import MagicMock, Mock

from django.test import TestCase
from django.contrib import messages
from django.contrib.admin.sites import AdminSite

from django_to_galaxy.admin.history import HistoryAdmin
from django_to_galaxy.models import History

from tests.factories.history import HistoryFactory


SINGULAR_STR = "singular"
PLURAL_STR = "plural"


class MockRequest:
    pass


class TestHistoryAdmin(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.galaxy_history = HistoryFactory()

    def setUp(self):
        self.site = AdminSite()

    def test_delete_queryset(self):
        # Given
        galaxy_history_admin = HistoryAdmin(History, self.site)
        # When
        # - Mock one queryset item
        queryset_item = MagicMock()
        queryset_item.delete.return_value = None
        test_queryset = [queryset_item]
        # - Call delete_queryset
        galaxy_history_admin.delete_queryset(MockRequest(), test_queryset)
        # Then
        queryset_item.delete.assert_called_once()

    def test_get_message_singular_history_synchronize(self):
        # Given
        galaxy_history_admin = HistoryAdmin(History, self.site)
        # When
        expected_string = (
            f"1 {SINGULAR_STR} [{self.galaxy_history.galaxy_id}"
            f" from {self.galaxy_history.galaxy_owner}]."
        )
        # - mock message_user method
        galaxy_history_admin.message_user = Mock()
        galaxy_history_admin.message_user.return_value = None
        # Then
        galaxy_history_admin._get_message_history_synchronize(
            MockRequest(),
            [self.galaxy_history],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_history_admin.message_user.call_args_list[0][0][1], expected_string
        )

    def test_get_message_plural_history_synchronize(self):
        # Given
        galaxy_history_admin = HistoryAdmin(History, self.site)
        # When
        expected_string = (
            f"2 {PLURAL_STR} [{self.galaxy_history.galaxy_id} from {self.galaxy_history.galaxy_owner}, "
            f"{self.galaxy_history.galaxy_id} from {self.galaxy_history.galaxy_owner}]."
        )
        # - mock message_user method
        galaxy_history_admin.message_user = Mock()
        galaxy_history_admin.message_user.return_value = None
        # Then
        galaxy_history_admin._get_message_history_synchronize(
            MockRequest(),
            [self.galaxy_history, self.galaxy_history],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_history_admin.message_user.call_args_list[0][0][1], expected_string
        )

    def test_synchronize_history(self):
        # Given
        galaxy_history_admin = HistoryAdmin(History, self.site)
        # When
        to_success_creation = Mock()
        to_success_creation.synchronize.return_value = None
        to_fail_creation = Mock()
        to_fail_creation.synchronize.side_effect = Exception()
        fake_queryset = [to_success_creation, to_fail_creation]
        # - mock method for message
        galaxy_history_admin._get_message_history_synchronize = Mock()
        galaxy_history_admin._get_message_history_synchronize.return_value = None
        # - call the method
        galaxy_history_admin.synchronize_history(MockRequest(), fake_queryset)
        # Then
        to_success_creation.synchronize.assert_called_once()
        to_fail_creation.synchronize.assert_called_once()
        galaxy_history_admin._get_message_history_synchronize.assert_called()
        self.assertEqual(
            galaxy_history_admin._get_message_history_synchronize.call_count, 2
        )
