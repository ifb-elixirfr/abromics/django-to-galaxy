from unittest.mock import patch, Mock

from django.test import TestCase
from django.contrib.admin.sites import AdminSite

from django_to_galaxy.admin.galaxy_instance import GalaxyInstanceAdmin
from django_to_galaxy.models import GalaxyInstance

from tests.factories.galaxy_instance import GalaxyInstanceFactory


class TestGalaxyUserAdmin(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.galaxy_instance = GalaxyInstanceFactory()

    def setUp(self):
        self.site = AdminSite()

    def test_get_registered_users(self):
        # Given
        galaxy_instance_admin = GalaxyInstanceAdmin(GalaxyInstance, self.site)
        # When
        link_str = "link_users"
        expected_link = (
            f'<a href="{link_str}?galaxy_instance__id={self.galaxy_instance.id}">0</a>'
        )
        mock_reverse = Mock()
        mock_reverse.return_value = link_str
        with patch("django_to_galaxy.admin.galaxy_instance.reverse", mock_reverse):
            # Then
            self.assertEqual(
                galaxy_instance_admin.get_registered_users(self.galaxy_instance),
                expected_link,
            )

    def test_is_online(self):
        # Given
        galaxy_instance_admin = GalaxyInstanceAdmin(GalaxyInstance, self.site)
        # When
        expected_output = False
        mock_is_online = Mock()
        mock_is_online.return_value = expected_output
        with patch(
            "django_to_galaxy.admin.galaxy_instance.GalaxyInstance.is_online",
            mock_is_online,
        ):
            # Then
            self.assertFalse(galaxy_instance_admin.is_online(self.galaxy_instance))
