from unittest.mock import Mock

from django.test import TestCase
from django.contrib import messages
from django.contrib.admin.sites import AdminSite

from django_to_galaxy.admin.galaxy_output_file import GalaxyOutputFileAdmin
from django_to_galaxy.models import GalaxyOutputFile

from tests.factories.galaxy_output_file import GalaxyOutputFileFactory


SINGULAR_STR = "singular"
PLURAL_STR = "plural"


class MockRequest:
    method = "GET"


class MockPostItem:
    def getlist(self, name: str):
        return ["test_id"]


class MockPostRequest(MockRequest):
    method = "POST"
    POST = MockPostItem()


class TestGalaxyUserAdmin(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.galaxy_output_file = GalaxyOutputFileFactory()

    def setUp(self):
        self.site = AdminSite()

    def test_get_message_singular_history_creation(self):
        # Given
        galaxy_output_file_admin = GalaxyOutputFileAdmin(GalaxyOutputFile, self.site)
        # When
        expected_string = f"1 {SINGULAR_STR} [{self.galaxy_output_file}]."
        # - mock message_user method
        galaxy_output_file_admin.message_user = Mock()
        galaxy_output_file_admin.message_user.return_value = None
        # Then
        galaxy_output_file_admin._get_message_output_file_synchronize(
            MockRequest(),
            [self.galaxy_output_file],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_output_file_admin.message_user.call_args_list[0][0][1],
            expected_string,
        )

    def test_get_message_plural_history_creation(self):
        # Given
        galaxy_output_file_admin = GalaxyOutputFileAdmin(GalaxyOutputFile, self.site)
        # When
        expected_string = (
            f"2 {PLURAL_STR} [{self.galaxy_output_file}, {self.galaxy_output_file}]."
        )
        # - mock message_user method
        galaxy_output_file_admin.message_user = Mock()
        galaxy_output_file_admin.message_user.return_value = None
        # Then
        galaxy_output_file_admin._get_message_output_file_synchronize(
            MockRequest(),
            [self.galaxy_output_file, self.galaxy_output_file],
            SINGULAR_STR,
            PLURAL_STR,
            messages.SUCCESS,
        )
        self.assertEqual(
            galaxy_output_file_admin.message_user.call_args_list[0][0][1],
            expected_string,
        )

    def test_synchronize_file(self):
        # Given
        galaxy_output_file_admin = GalaxyOutputFileAdmin(GalaxyOutputFile, self.site)
        # When
        to_success_action = Mock()
        to_success_action.synchronize.return_value = None
        to_fail_action = Mock()
        to_fail_action.synchronize.side_effect = Exception()
        fake_queryset = [to_success_action, to_fail_action]
        # - mock method for message
        galaxy_output_file_admin._get_message_output_file_synchronize = Mock()
        galaxy_output_file_admin._get_message_output_file_synchronize.return_value = (
            None
        )
        # - call the method
        galaxy_output_file_admin.synchronize_file(MockRequest(), fake_queryset)
        # Then
        to_success_action.synchronize.assert_called_once()
        to_fail_action.synchronize.assert_called_once()
        galaxy_output_file_admin._get_message_output_file_synchronize.assert_called()
        self.assertEqual(
            galaxy_output_file_admin._get_message_output_file_synchronize.call_count, 2
        )
