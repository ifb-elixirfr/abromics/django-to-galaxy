import unittest

from django_to_galaxy.version import __version__


class TestVersion(unittest.TestCase):
    def test_version_number(self):
        self.assertEqual(__version__, "0.6.9.6")
