.. django-to-galaxy-documentation documentation master file, created by
   sphinx-quickstart on Tue Jul 12 14:31:28 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-to-galaxy API documentation!
==============================================

This is a Django extension that eases communication with Galaxy instance to execute workflows.

.. _Wiki: https://gitlab.com/ifb-elixirfr/abromics/django-to-galaxy/-/wikis/home

.. toctree::
    :caption: User Guide
    :maxdepth: 1

    user_guide/configuration
    user_guide/galaxy_interaction

.. toctree::
    :caption: Contribution Guide
    :maxdepth: 1

    contribution_guide/run_project_locally
    contribution_guide/run_tests_locally
    contribution_guide/factories
    contribution_guide/documentation

.. toctree::
    :caption: API Documentation
    :maxdepth: 1

    api_docs/models
    api_docs/schemas
    api_docs/utils
