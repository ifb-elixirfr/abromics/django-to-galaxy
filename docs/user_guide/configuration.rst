.. _user_guide_configuration:

****************
🔧 Configuration
****************

⚙️ Django settings
==================

First, you need to add the extension in the `INSTALLED_APPS` of your `settings.py`:

.. code-block:: python

    INSTALLED_APPS = [
        "django_to_galaxy",
        ...
    ]

🔥 Endpoints from Django REST framework
=======================================

Django REST framework
----------------------

To use Django REST framework in your application, you need to add `rest_framework` extension in the `INSTALLED_APPS` of your `settings.py`:

.. Note::
    We would recommend to have a look at the
    `check drf documentation <https://www.django-rest-framework.org/tutorial/quickstart/#settings>`_
    for more information.

.. code-block:: python

    INSTALLED_APPS = [
        "rest_framework",
        ...
    ]

Then, in your main `urls.py` you can add the prebuilt endpoints from `django_to_galaxy`:

.. code-block:: python

    from django_to_galaxy.api.urls import api_router

    urlpatterns = [
        ...
        path("your/root/path/", include(api_router.urls)),
    ]

You can also design your own endpoints and starts from the different available views in `django_to_galaxy.api.views`.

.. Note::
    We would recommend to have a look at the whole code, especially `django_to_galaxy.api`
    to help you build your own endpoints.

Automatic Swagger documentation
""""""""""""""""""""""""""""""""

Also for documentation, you need to add `drf_yasg` extension in the `INSTALLED_APPS` of your `settings.py`:

.. code-block:: python

    INSTALLED_APPS = [
        "drf_yasg",
        ...
    ]

Then, you can add the following routes to your `urls.py`:

.. code-block:: python

    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi

    urlpatterns = [
        ...
        re_path(
            r"^swagger(?P<format>\.json|\.yaml)$",
            schema_view.without_ui(cache_timeout=0),
            name="schema-json",
        ),
        re_path(
            r"^swagger/$",
            schema_view.with_ui("swagger", cache_timeout=0),
            name="schema-swagger-ui",
        )
    ]

.. Note::
    We would recommend to have a look at the 
    `drf_yasg documentation <https://drf-yasg.readthedocs.io/en/stable>`_
    for more information.
