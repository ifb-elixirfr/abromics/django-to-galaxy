.. _user_guide_galaxy_interaction:

***********************
🛸 Interact with Galaxy
***********************

.. Note::
    Most of the configuration for Galaxy goes through the Django admin of your application.

💻 Galaxy instance
==================

You first need to add your Galaxy instance in the admin panel.

.. Note::
    The path should be `{root_address}/admin/django_to_galaxy/galaxyinstance/`

👤 Galaxy user
==============

Then you can create the user you need to communication with galaxy. This is at this moment you specify you Galaxy API key.

.. Note::
    `How to get your Galaxy API key <https://training.galaxyproject.org/training-material/faqs/galaxy/preferences_admin_api_key.html>`_

.. Note::
    The path should be `{root_address}/admin/django_to_galaxy/galaxyuser/`

Django admin actions
---------------------

From the admin panel of users, you have several actions to interact with the galaxy instances from the different users.

Create History
""""""""""""""

This create a history on the Galaxy instance for selected users. At the moment,
the name is fixed but will be meaningful after
`this issue <https://gitlab.com/ifb-elixirfr/abromics/django-to-galaxy/-/issues/8>`_ will be done.

.. Note::
    If you delete a history item on the django side, it will also delete it on galaxy side.
    We consider that every history used on the django side has to be created from django to be used.

Import Workflow(s)
""""""""""""""""""

.. Note::
    This action needs to be performed on one user only

It allows addition and import of workflows from the galaxy instance of the selected user:

* Select one user (and only one)
* Open list of actions and select `Import workflow(s) (1 user only)`
* (Page takes few seconds to load) select workflow(s) to import from the list by checking the boxes
* Click `Import selected workflow(s)`
* That's it!

API
---

Create History
""""""""""""""

Use the route ``/django_to_galaxy/api/create_history/<user_id>`` to generate history for a given user.


📔 History
==========

API
---

Upload file to history
""""""""""""""""""""""

**API endpoint**: ``django_to_galaxy/api/upload_to_history/``

* In the payload of the API, you give the ``id`` of the history
* For the file, you give a nested payload with:
  
    * ``model``: the slug name of the model used by the app to store the file locally on Django

    * ``field_path``: the field used for the file (either ``FileField`` or ``FilePathField``)

    * ``id``: the ID of the file to retrieve

Django admin actions
--------------------

Synchronize data from Galaxy
"""""""""""""""""""""""""""""

It allows to synchronize the data in the django tables from the histories in Galaxy.

♻️ Workflow
===========

Interact with workflow
-----------------------

How to invoke workflow from the terminal
"""""""""""""""""""""""""""""""""""""""""

From the command line
''''''''''''''''''''''

At the moment, workflow can be invoked using python uniquely

.. Note::
    Here is the `corresponding MR <https://gitlab.com/ifb-elixirfr/abromics/django-to-galaxy/-/merge_requests/7>`_.


If the workflow takes as input only one file that was uploaded to history

.. code-block:: python

    from django_to_galaxy.models import History, Workflow
    # Retrieve your workflow and history
    wf = Workflow.objects.get(galaxy_id="474fc4a321a8ef55")
    h = History.objects.get(galaxy_id="bca59e65293f7341")
    # Generate datamap
    datamap = h.simplify_datasets[0].generate_datamap('0')
    # Invoke
    inv = wf.invoke(datamap, history=h)



Using the API
'''''''''''''

* **API endpoint**: `django_to_galaxy/api/invoke_workflow/`

* In the payload of the API, you give the ``id`` of the history and the ``id`` of the workflow

    * ``galaxy_id``

    * ``history_id``


* You also specify the ``datamap`` used to invoke the workflow, pre-built from the History

* To get the required datamap template for a given workflow use the route ``django_to_galaxy/api/get_datamap_template/<workflow_id>``


⏳ Invocation
=============

Django admin actions
---------------------

Synchronize data from Galaxy
""""""""""""""""""""""""""""

It allows to synchronize the data in the django tables from the histories in Galaxy.

Update output files
"""""""""""""""""""

Update list of output galaxy files from an Invocation

API
---

Update output files
"""""""""""""""""""

Use the route ``/django_to_galaxy/api/update_galaxy_output_files/<invocation_id>`` to update output files from a given invocation.

