.. _contribution_factories:

************
🏭 Factories
************

.. Note::
    Factories are built using ``Factory Boy``.
    Please have a look at the `documentation <https://factoryboy.readthedocs.io/en/stable/index.html>`_
    for more information.

To prevent writing all entries to feed the database every time you need to write a test,
factories for the different models are available in the ``tests/factories`` directory.

In brief, it allows you to create an entry on the flight by instanciation the factory.
It also deal with creating entries related to your entry by a foreign key
(`example <https://factoryboy.readthedocs.io/en/stable/examples.html#factories>`_).

🔍 Example
==========

The following lines generate both a GalaxyUser and GalaxyInstance entry.
Value are randomly generated using the `Faker` library.

.. code-block:: python

    from tests.factories.galaxy_user import GalaxyUserFactory

    user = GalaxyUserFactory()
