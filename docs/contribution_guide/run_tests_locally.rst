.. _contribution_run_tests:

******************************
🚧 Tests (CI/CD rules) locally
******************************

.. Note::
    `Invoke <https://www.pyinvoke.org/>`_ is used to wrap the different CI/CD rules.

You can run all quality checks and unit tests locally. To list all available commands:

.. code-block:: bash

    poetry run inv --list
    # for instance
    poetry run inv quality.all  # Run all quality tests


💎 Quality
==========

black and flake8 are applied on this project and you can run all using:

.. code-block:: bash

    poetry run inv quality.all


🚦 Tests
========

You can run unit tests using:

.. code-block:: bash

    poetry run inv tests.unit
    poetry run inv tests.cov # with coverage
