.. _contribution_run_locally:

****************************************
💻 Run the project on your local machine
****************************************

🔧 Dependencies
===============

Packaging and organization of the library is done using `Poetry <https://python-poetry.org/docs/>`_.
(`Poetry install instructions <https://python-poetry.org/docs/#installation>`_).

💿 Install `django-to-galaxy`
=============================

.. code-block:: bash

    poetry install

To activate the corresponding virtual environment created by poetry:

.. code-block:: bash
    
    poetry shell

.. note::
    This can be useful to retrieve the absolute python path for VScode for instance (`which python`)

💻 Run the testing Django app locally
=====================================

A testing django app is available locally to help you through development in the `testing_django_app` directory:

.. code-block:: bash

    poetry shell
    python testing_django_app/manage.py runserver
