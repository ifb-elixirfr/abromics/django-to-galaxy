.. _api_utils:

********
🔧 Utils
********

.. currentmodule:: django_to_galaxy.utils

Galaxy parsing
==============

.. autosummary::
    :toctree: stubs

    load_galaxy_time_to_datetime
    load_galaxy_history_time_to_datetime
    load_galaxy_invocation_time_to_datetime

Tool dev
========

.. currentmodule:: django_to_galaxy.utils

.. autosummary::
    :toctree: stubs

    enabled_cache