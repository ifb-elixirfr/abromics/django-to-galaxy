.. _api_models:

*********
📄 Models
*********

.. currentmodule:: django_to_galaxy.models

Galaxy configuration
====================

.. autosummary::
    :toctree: stubs

    GalaxyInstance
    GalaxyUser

Galaxy elements
===============

.. currentmodule:: django_to_galaxy.models

.. autosummary::
    :toctree: stubs

    History
    Workflow
    Invocation
    GalaxyOutputFile
