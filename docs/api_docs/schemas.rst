.. _api_schemas:

**********
📄 Schemas
**********

.. currentmodule:: django_to_galaxy.schemas.dataset

Dataset
=======

.. autosummary::
    :toctree: stubs

    SimpleDataset
